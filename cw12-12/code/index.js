/*
Використовуючи CallBack function створіть калькулятор, який буде від користувача приймати 2 числа і знак арефметичної операції. При введенні не числа або при розподілі на 0 виводити помилку.
*/

const getData = () => {
    const operand1 = parseFloat(prompt("Введіть число 1", "0-1000"));
    const operand2 = parseFloat(prompt("Введіть число 2", "0-1000"));
    const sign = prompt("Введіть знак операції", "+-/*");
    return {
        operand1: operand1, operand2: operand2, sign: sign
    }
}

function calculate(data) {
    switch (data.sign) {
        case "+": return add(data.operand1, data.operand2);
        case "-": return sub(data.operand1, data.operand2);
        case "/": return div(data.operand1, data.operand2);
        case "*": return mul(data.operand1, data.operand2);
        default: console.error("Щось пішло не так:(")
    }
}

const add = (a, b) => a + b;
const sub = (a, b) => a - b;
const div = (a, b) => {
    if (b === 0) {
        throw new Error("❌ Ділення на 0 😤");
    }
    return a / b
}
const mul = (a, b) => a * b;

function show (rez) {
    document.getElementById("rez").innerHTML = rez;
}

show(calculate(getData()));

